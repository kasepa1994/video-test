export const data1 = [{
    author: 'Xiangwj',
    title: '# 航拍达人',
    info: '带着我的无人机，飞过城市高空、跨过山和大海',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-1.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 铁血柔情',
    info: '带你了解军人的​​​​​铁血柔情',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-2.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 飞翔者',
    info: '我想知道天空的颜色',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-3.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 大咖伴你游',
    info: '爱旅行、爱运动、旅行达人带你游遍全球',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-4.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 全球游记',
    info: '人生得意须尽欢',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-5.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 夜景达人',
    info: '带你看遍夜景',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-6.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 大白看世界',
    info: '大白的旅行',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-7.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 健助师小哥',
    info: '我是健身达人，快点跟上我',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-8.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 喵小姐',
    info: '我想带去看浪漫的土耳其',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-9.jpg',
    isJoin: false
}, {
    author: 'Xiangwj',
    title: '# 你好小姐姐',
    info: '带你吃喝玩乐',
    img: 'http://cdn.emas-poc.com/material/emas-showcase/uz-10.jpg',
    isJoin: false
}, ]

export const data2 = [{
    title: '粉丝',
    img: 'http://cdn.emas-poc.com/material/emasyanshichanpin/icon_SP@3x.png',
}, {
    title: '赞',
    img: 'http://cdn.emas-poc.com/material/emasyanshichanpin/icon_DZ@3x.png',
}, {
    title: '评论',
    img: 'http://cdn.emas-poc.com/material/emasyanshichanpin/icon_FS@3x.png',
}, {
    title: '随拍',
    img: 'http://cdn.emas-poc.com/material/emasyanshichanpin/icon_PL@3x.png',
}]